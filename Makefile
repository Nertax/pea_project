CC=g++
CFLAGS=-O2 -Wall -Wextra -Wconversion -pedantic -std=c++11 
OBJ=main.o Tsp.o Menu.o Measure.o

projekt: $(OBJ)
	$(CC)  $(CFLAGS) -o projekt $^ 
	

%.o: %.cpp
	$(CC) -c $< -o $@ $(CFLAGS) 

clean:
	rm -f *.o


