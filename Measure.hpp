#include<iostream>
#include<fstream>
#include<queue>
#include"Tsp.hpp"


#ifndef Measure_h
#define Measure_h


enum MeasuerTSPAlgorithms {

    BruteForce,
    HeldKarp,
    BnBRow,
    BnBRowColumn,
    BnBMax

};


class Measure {

    private:
        void _AutoMeasurements();
        int _ExecuteMeasureTSP(uint numberOfMeasurements, uint8_t problemSize, std::string fileName, MeasuerTSPAlgorithms algorithm);
        long _MeasureTSP(uint8_t problemSize, MeasuerTSPAlgorithms algorithm);
        long _TimeDiff(timespec start, timespec end);
   
    public:

        void Start();
};

#endif
