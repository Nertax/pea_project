#include"Tsp.hpp"

void TSP::generateRandomCitiesMatrix(uint8_t matrixSize) {

    std::mt19937 randomEngine(time(0));
    std::uniform_int_distribution<int> distribution(1, std::numeric_limits<int>::max());
    auto generator = std::bind(distribution, randomEngine);


    citiesAmount = matrixSize;

    citiesMatrix.resize(matrixSize);
                       
    for(size_t i = 0; i < matrixSize; ++i)
        citiesMatrix[i].resize(matrixSize);

    for(size_t i = 0; i < matrixSize; ++i)
        for(size_t j = 0; j < matrixSize; ++j) 
        {
            if(i == j)
                citiesMatrix[i][j] = 0;
            else
                citiesMatrix[i][j] = generator();
        }

}


void TSP::printCitiesMatrix() {

    if(citiesMatrix.empty())
        std::cout << "City Matrix is empty!" << std::endl;
    else 
    {
        for(std::vector<int> row : citiesMatrix) 
        {
            for(int x : row)
                std::cout << x << " ";

            std::cout << std::endl;
        }
    }

}

void TSP::readMatrixFromFile(std::string fileName) {


    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        int temp;
        file >> temp;
        citiesAmount = temp;
        if(file.fail()) 
            std::cout << "Error - can't read matrix size" << std::endl; 
        else {
        
            citiesMatrix.resize(citiesAmount);
                       
            for(size_t i = 0; i < citiesAmount; ++i)
                citiesMatrix[i].resize(citiesAmount);

            for(size_t i = 0; i < citiesAmount; ++i)
                for(size_t j = 0; j < citiesAmount; ++j) 
                {

                    file >> citiesMatrix[i][j];

                    if(file.fail())
                    {
                        std::cout << "Error during reading matrix content" << std::endl; 
                        file.close(); 
                        return; 
                    } 
                }
        }

            std::cout << "Reading complete!" << std::endl; 
            file.close(); 
    }
    else 
        std::cout << "Error - can't open file with name: " << fileName << std::endl; 

    
}

void TSP::setStartCity(uint8_t cityNumber) {

    if(cityNumber >= citiesAmount)
        std::cout << "Error - start city number greater than amount of cities" << std::endl;
    else
        startCity = cityNumber;

}

TSPresult TSP::bruteForce() {


    TSPresult result;
    
    int64_t weight = 0;
    std::vector<uint8_t> path; 

    for(uint8_t i = 0; i < citiesAmount; i++)
        if(i != startCity)
            path.push_back(i);


    weight += citiesMatrix[startCity][path[0]];

    for(size_t i = 0; i < path.size() - 1; i++)
        weight += citiesMatrix[path[i]][path[i+1]];

    weight += citiesMatrix[path.back()][startCity];

    result.shortestPath = path;
    result.pathWeight = weight;


    while(std::next_permutation(path.begin(), path.end())) {

        weight = 0;
        weight += citiesMatrix[startCity][path[0]];

        for(size_t i = 0; i < path.size() - 1; i++)
            weight += citiesMatrix[path[i]][path[i+1]];

        weight += citiesMatrix[path.back()][startCity];

        if(weight < result.pathWeight) {
            result.pathWeight = weight;
            result.shortestPath = path;
        }

    }


    return result;
}

std::string TSPresult::toString(uint8_t startCity) {


    std::string s = "Path weight: " + std::to_string(this->pathWeight) + '\n';
    s = s + "Path: " + std::to_string(startCity) + " - ";

    for(uint city : this->shortestPath)
        if(city != startCity) //bnb adds start city to path so we need to check it to avoid double printing of startCity
            s = s + std::to_string(city) + " - ";

    s += std::to_string(startCity);

    return s;


}


bool TSP::ifVectorContain(std::vector<uint8_t> vect, uint8_t value) {

    for(size_t i = 0; i < vect.size(); i++) 
        if(vect[i] == value)
            return true;

    
    return false;
}


std::vector<uint8_t> TSP::getVectorWithoutElement(std::vector<uint8_t> vect, uint8_t value) {

    std::vector<uint8_t> newVect(vect);
    for(size_t i = 0; i < newVect.size(); i++) {
        if(newVect[i] == value) {
            newVect.erase(newVect.begin() + i);
            return newVect;
        }
    }    
}



TSPresult TSP::heldKarp() {


    TSPresult result;
    std::map<CityWithLeftCities, WeightAndPrevious> mapOfReadyNodes;

    
    //from 1 becasue 0 is our start city
    for(uint8_t i = 1; i < citiesAmount; i++) {

        std::vector<uint8_t> temp;
        mapOfReadyNodes[CityWithLeftCities(i, temp)] = WeightAndPrevious(citiesMatrix[i][startCity], startCity);
    }

    Subset::setLength(citiesAmount); // set length - set size of the set from we will make subsets

    for(int subsetLength = 1; subsetLength < citiesAmount - 1; subsetLength++) { 
        Subset::resetVectorOfSubsets();
        std::vector<std::vector<uint8_t>> temp = Subset::getVectorOfSubsets(subsetLength); // make vector of all subsets of size subsetLength
        
        for(size_t m = 0; m < temp.size(); m++) { //g(?,|{3}|{4}|) = c23 + g(3, ∅ ) = c23 + c31 = 6 + 15 = 21

            if(!ifVectorContain(temp[m], startCity)) {
                for(uint8_t j = 0; j < citiesAmount; j++) { //g(|2|4|,{3}) = c23 + g(3, ∅ ) = c23 + c31 = 6 + 15 = 21
   
                    if(!ifVectorContain(temp[m], j) && j != startCity) { 

                        int64_t tempPath;
                        mapOfReadyNodes[CityWithLeftCities(j, temp[m])] = WeightAndPrevious(std::numeric_limits<int64_t>::max(), 0);

                        for(size_t k = 0; k < temp[m].size(); k++) {
    
                            tempPath = citiesMatrix[j][temp[m][k]] + mapOfReadyNodes[CityWithLeftCities(temp[m][k], getVectorWithoutElement(temp[m], temp[m][k]))].Weight;

                            if(tempPath < mapOfReadyNodes[CityWithLeftCities(j, temp[m])].Weight) {
                                mapOfReadyNodes[CityWithLeftCities(j, temp[m])].Weight = tempPath;
                                mapOfReadyNodes[CityWithLeftCities(j, temp[m])].PreviousCity = temp[m][k];
                            }
                        }
                    }                                        
                }    
            }  
        }
    }

    //std::cout << "Map size: " << mapOfReadyNodes.size() << std::endl;

    
    int64_t res = std::numeric_limits<int64_t>::max();
    uint8_t prev = 0;

    int64_t tempRes;

    std::vector<uint8_t> vectorWithoutStartCity;

    //make vector of all cities without start one
    for(uint8_t i = 0; i < citiesAmount; i++)
        if(i != startCity)
            vectorWithoutStartCity.push_back(i);

    //last step of Held Karp
    for(size_t i = 0; i < vectorWithoutStartCity.size(); i++) {

        tempRes = citiesMatrix[startCity][vectorWithoutStartCity[i]] + mapOfReadyNodes[CityWithLeftCities(vectorWithoutStartCity[i], getVectorWithoutElement(vectorWithoutStartCity, vectorWithoutStartCity[i]))].Weight;

        if(tempRes < res) {
            res = tempRes;
            prev = vectorWithoutStartCity[i];
        }
    }


    result.pathWeight = res;

    while(!vectorWithoutStartCity.empty()) {

        result.shortestPath.push_back(prev);
        vectorWithoutStartCity = getVectorWithoutElement(vectorWithoutStartCity, prev);
        prev = mapOfReadyNodes[CityWithLeftCities(prev, vectorWithoutStartCity)].PreviousCity;

    }

    return result;
}

TSPresult TSP::BnBDFS(int64_t (TSP::*getBound)(TSPnode)) {


    std::priority_queue<TSPnode> leastBoundQueue;


    TSPresult res;
    res.pathWeight = std::numeric_limits<int64_t>::max();
    
    // unsigned long queueElemAmount = 0;

    //make first node and add it to priority queue
    std::vector<uint8_t> citiesLeftFromStartCity;

    for(uint8_t i = 1; i < citiesAmount; i++) {
        citiesLeftFromStartCity.push_back(i);

    }


    TSPnode firstNode(startCity, citiesLeftFromStartCity);
    firstNode.Weight = 0;

    firstNode.Bound = (this->*getBound)(firstNode); 

    leastBoundQueue.push(firstNode);

    while(!leastBoundQueue.empty()) {


        if(leastBoundQueue.top().Bound > res.pathWeight)
            break;
        else 
        {
            if(leastBoundQueue.top().CitiesLeft.size() == 1)
            {
                int64_t tempWeight = leastBoundQueue.top().Weight + citiesMatrix[leastBoundQueue.top().City][leastBoundQueue.top().CitiesLeft[0]] + citiesMatrix[leastBoundQueue.top().CitiesLeft[0]][startCity];

                if(tempWeight < res.pathWeight) 
                {
                    res.pathWeight = tempWeight;
                    res.shortestPath = leastBoundQueue.top().Path;
                    res.shortestPath.push_back(leastBoundQueue.top().City);
                    res.shortestPath.push_back(leastBoundQueue.top().CitiesLeft.front());

                }

                leastBoundQueue.pop();
            }
            else 
            {

                TSPnode currentlyLeastBoundNode(leastBoundQueue.top());
                leastBoundQueue.pop();

                for(size_t i = 0; i < currentlyLeastBoundNode.CitiesLeft.size(); i++)
                {
                    
                    TSPnode node(currentlyLeastBoundNode.CitiesLeft[i], getVectorWithoutElement(currentlyLeastBoundNode.CitiesLeft, currentlyLeastBoundNode.CitiesLeft[i]), currentlyLeastBoundNode.Path);
                    node.Path.push_back(currentlyLeastBoundNode.City);
                    node.Weight = currentlyLeastBoundNode.Weight + citiesMatrix[currentlyLeastBoundNode.City][currentlyLeastBoundNode.CitiesLeft[i]];
                    
                    node.Bound = (this->*getBound)(node); 

                    if(node.Bound < res.pathWeight)
                    {
                        leastBoundQueue.push(node);

                       // if(leastBoundQueue.size() > queueElemAmount)
                        //    queueElemAmount = leastBoundQueue.size();
                    }

                }
            }
        }
    }

    //std::cout << "Max queue elem amount: " << queueElemAmount << std::endl;

    return res;
}


int64_t TSP::getBoundByMinValueInEachRow(TSPnode node) 
{

    int64_t bound = node.Weight;
    int temp = std::numeric_limits<int>::max();

    for(uint8_t i = 0; i < citiesMatrix.size(); i++)
        if(citiesMatrix[node.City][i] < temp)
            if(!ifVectorContain(node.Path, i))
                temp = citiesMatrix[node.City][i];

    bound += temp;
    

    for(size_t i = 0; i < node.CitiesLeft.size(); i++)
    {

        temp = std::numeric_limits<int>::max();

        for(uint8_t j = 0; j < citiesMatrix.size(); j++)        
            if(citiesMatrix[node.CitiesLeft[i]][j] < temp && node.CitiesLeft[i] != j)
                if(!ifVectorContain(node.Path, j) || j == 0)
                    temp = citiesMatrix[node.CitiesLeft[i]][j];
            

        bound += temp;
    }
 
    return bound;
}

int64_t TSP::getBoundByAvgOfMinValueInRowAndColumn(TSPnode node) 
{
    int64_t bound = node.Weight;
    double minInRow = std::numeric_limits<double>::max();
    double minInColumn = std::numeric_limits<double>::max();

    //exit from City
    for(uint8_t i = 0; i < node.CitiesLeft.size(); i++)
        if(citiesMatrix[node.City][node.CitiesLeft[i]] < minInRow)
                minInRow = citiesMatrix[node.City][node.CitiesLeft[i]];

    //entrance back to startCity 
    for(uint8_t i = 0; i < node.CitiesLeft.size(); i++)
        if(citiesMatrix[node.CitiesLeft[i]][startCity] < minInColumn)
                minInColumn = citiesMatrix[node.CitiesLeft[i]][startCity];

    bound += (int64_t)((minInRow + minInColumn) / 2);



    for(size_t i = 0; i < node.CitiesLeft.size(); i++)
    {

       minInRow = citiesMatrix[node.CitiesLeft[i]][startCity];;
       minInColumn = citiesMatrix[node.City][node.CitiesLeft[i]];


        //entrance to node
        for(uint8_t j = 0; j < node.CitiesLeft.size(); j++) {
            if(citiesMatrix[node.CitiesLeft[j]][node.CitiesLeft[i]] < minInColumn && i != j) {
                    minInColumn = citiesMatrix[node.CitiesLeft[j]][node.CitiesLeft[i]];
            }
        }

        //exit from node
        for(uint8_t j = 0; j <  node.CitiesLeft.size(); j++) {
            if(citiesMatrix[node.CitiesLeft[i]][node.CitiesLeft[j]] < minInRow && i != j) {
                    minInRow = citiesMatrix[node.CitiesLeft[i]][node.CitiesLeft[j]];
            }
        }

        bound += (int64_t)((minInRow + minInColumn) / 2);
    
    }

    return bound;
}

int64_t TSP::getMaxBound(TSPnode node) {

    int64_t boundA = this->getBoundByAvgOfMinValueInRowAndColumn(node);
    int64_t boundB = this->getBoundByMinValueInEachRow(node);

    if(boundA > boundB)
        return boundA;
    else
        return boundB;

}



int Subset::Length = 0;
std::vector<std::vector<uint8_t>> Subset::vectorOfSubsets;

void Subset::setLength(int length) {

    Length = length;

}

void Subset::output(int string[], int position)
{

    bool * temp_string = new bool[Length];
    int index = 0;
    uint8_t i;
    for (i = 0; i < Length; i++)
    {
        if ((index < position) && (string[index] == i))
        {
            temp_string[i] = 1;
            index++;
        }
        else
            temp_string[i] = 0;
    }


    std::vector<uint8_t> temp;

    for (i = 0; i < Length; i++)
        if(temp_string[i] == true) 
            temp.push_back(i);
        

    delete [] temp_string;

    vectorOfSubsets.push_back(temp);

}
// Recursively generate the banker’s sequence.
void Subset::generate(int string[], int position, int positions)
{

    if (position < positions)
    {  
        if (position == 0)
        {
            for (int i = 0; i < Subset::Length; i++)
            {
                string[position] = i;
                generate(string, position + 1, positions);
            }
        }
        else
        {
            for (int i = string[position - 1] + 1; i < Subset::Length; i++)
            {
                string[position] = i;
                generate(string, position + 1, positions);
             }
        }
    }
    else
        output(string, positions);
}

std::vector<std::vector<uint8_t>> Subset::getVectorOfSubsets(int subsetSize) { 

    int * string = new int[Length]; 
    generate(string, 0, subsetSize);
    delete [] string; 
    return vectorOfSubsets; 
}