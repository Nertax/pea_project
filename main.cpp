#include"Tsp.hpp"
#include<iostream>
#include"Menu.hpp"
#include"Measure.hpp"

void PrintHelp();

int main(int argc, char **argv) {

  if(argc != 2)
    PrintHelp();
  else {

    std::string str(argv[1]);
    if(str.compare("-m") == 0) {

       Measure mainMeasure;
       mainMeasure.Start();

    }
    else if(str.compare("-t") == 0) {
     
      Menu mainMenu;
      mainMenu.Start();
    }
    else 
      PrintHelp();

  } 

    return 0;

}

void PrintHelp() {

  std::cout << "Wywolaj program z opcja -m do automatycznych pomiarow, lub -t dla reczego uzywania algorytmow" << std::endl;

}
