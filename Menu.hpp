#include<iostream>
#include<string>
#include<algorithm>
#include"Tsp.hpp"


#ifndef Menu_h
#define Menu_h

class Menu {

    private:

        TSP tsp;

        void _MenuLoop();
        void _PrintHelp();
        void _PrintNoSuchAction();
        void _PrintStart();

    public:
        void Start();
};

#endif
