#include"Menu.hpp"

void Menu::Start() {

    _PrintStart();
    _MenuLoop();
}


void Menu::_MenuLoop() {

    std::string action, fileName;
    int a;

    while(1) {

        
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);


        if(action == "h" || action == "help")
            _PrintHelp();

        else if(action == "p" || action == "print")
            tsp.printCitiesMatrix();

        else if(action == "r" || action == "read")
        {
            std::cin >> fileName;
            tsp.readMatrixFromFile(fileName);
        }

        else if(action == "g" || action == "generate")
        {
            std::cin >> a;
            if(a < std::numeric_limits<uint8_t>::max())
                tsp.generateRandomCitiesMatrix(a);
            else
                std::cout << "Matrix size must be less than 255." << std::endl;
        }

        else if(action == "b" || action == "bruteforce")
            std::cout << "Brute force: " << tsp.bruteForce().toString(tsp.getStartCity()) << std::endl;

        else if(action == "hk" || action == "heldkarp")
            std::cout << "Held Karp: " << tsp.heldKarp().toString(tsp.getStartCity()) << std::endl;

        else if(action == "bbr" || action == "bnbrow")
            std::cout << "BnB DFS in row: " << tsp.BnBDFS(&TSP::getBoundByMinValueInEachRow).toString(tsp.getStartCity()) << std::endl;

        else if(action == "bbrc" || action == "bnbrowcolumn")
            std::cout << "BnB DFS in row and column: " << tsp.BnBDFS(&TSP::getBoundByAvgOfMinValueInRowAndColumn).toString(tsp.getStartCity()) << std::endl;

        else if(action == "bbm" || action == "bnbmax")
            std::cout << "BnB DFS max: " << tsp.BnBDFS(&TSP::getMaxBound).toString(tsp.getStartCity()) << std::endl;

        else if(action == "q" || action == "quit")
            return;

        else
            _PrintNoSuchAction();

    }


}

void Menu::_PrintStart() {
    std::cout << "Program has been started in test mode - you can now test correctness of tsp algorithms." << std::endl << std::endl;
}

void Menu::_PrintNoSuchAction() {

    std::cout << "There is not such action, enter h/help for more information." << std::endl << std::endl;
}


void Menu::_PrintHelp() {


    std::cout << "This message - h/help" << std::endl
              << "Generate random matrix of cities - g/generate + <matrix size> " << std::endl
              << "Print matrix of cities - p/print" << std::endl
              << "Read matrix from file - r/read + <file name>" << std::endl
              << "Brute force - b/bruteforce" << std::endl
              << "Held Karp - hk/heldkarp" << std::endl
              << "BnB DFS LC 1 - bbr/bnbrow" << std::endl
              << "BnB DFS LC 2 - bbrc/bnbrowcolumn" << std::endl
              << "BnB DFS LC 3 - bbm/bnbmax" << std::endl
              << "Quit - q/quit" << std::endl;

}

