#include"Measure.hpp"

void Measure::Start() {

    _AutoMeasurements();
}



void Measure::_AutoMeasurements() {

    uint numberOfMeasurements = 10;

    std::queue<uint8_t> structureSizeList;

    // structureSizeList.push(4);
    // structureSizeList.push(5);
    // structureSizeList.push(6);
    // structureSizeList.push(7);
    // structureSizeList.push(8);
    // structureSizeList.push(9);
    // structureSizeList.push(10);
    // structureSizeList.push(11);
    // structureSizeList.push(12);
    structureSizeList.push(22);
    structureSizeList.push(24);
    structureSizeList.push(26);
    structureSizeList.push(28);
    structureSizeList.push(30);
    // structureSizeList.push(18);



    while(!structureSizeList.empty()) {

       // if(_ExecuteMeasureTSP(numberOfMeasurements, structureSizeList.front(), "bf_100", MeasuerTSPAlgorithms(BruteForce)) != 1) break;
        //if(_ExecuteMeasureTSP(numberOfMeasurements, structureSizeList.front(), "hk_10", MeasuerTSPAlgorithms(HeldKarp)) != 1) break;
        //if(_ExecuteMeasureTSP(numberOfMeasurements, structureSizeList.front(), "bnbr_10", MeasuerTSPAlgorithms(BnBRow)) != 1) break;
        if(_ExecuteMeasureTSP(numberOfMeasurements, structureSizeList.front(), "bnbrc_10", MeasuerTSPAlgorithms(BnBRowColumn)) != 1) break;
        //if(_ExecuteMeasureTSP(numberOfMeasurements, structureSizeList.front(), "bnbm_10", MeasuerTSPAlgorithms(BnBMax)) != 1) break;


        std::cout << "Dokonano pomiarow dla problemu o wielkosci " << (int)structureSizeList.front() << std::endl;
        structureSizeList.pop();

    }

    std::cout << "Zakonczono automatyczne pomiary." << std::endl;
}




int Measure::_ExecuteMeasureTSP(uint numberOfMeasurements, uint8_t problemSize, std::string fileName, MeasuerTSPAlgorithms algorithm) {

    std::fstream file;
    long timeMeasureResult;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << (int)problemSize << ";;";
            while(numberOfMeasurements--) {

                timeMeasureResult = _MeasureTSP(problemSize, algorithm);
                
                if(timeMeasureResult <= -1) {
                    std::cout << "Blad podczas pomiaru, przerywam pomiary" << std::endl;
                    return 0; 

                }

                file << timeMeasureResult << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    return -1;
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else {

        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
        return -2;
    }

    return 1;
    

}



long Measure::_MeasureTSP(uint8_t problemSize, MeasuerTSPAlgorithms algorithm) {

    TSP tsp;
    struct timespec a, b;
    tsp.generateRandomCitiesMatrix(problemSize); 

    if(algorithm == MeasuerTSPAlgorithms(BruteForce)) {

        clock_gettime(CLOCK_REALTIME, &a);
        tsp.bruteForce();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(algorithm == MeasuerTSPAlgorithms(HeldKarp)) {

        clock_gettime(CLOCK_REALTIME, &a);
        tsp.heldKarp();
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(algorithm == MeasuerTSPAlgorithms(BnBRow)) {

        clock_gettime(CLOCK_REALTIME, &a);
        tsp.BnBDFS(&TSP::getBoundByMinValueInEachRow);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(algorithm == MeasuerTSPAlgorithms(BnBRowColumn)) {

        clock_gettime(CLOCK_REALTIME, &a);
        tsp.BnBDFS(&TSP::getBoundByAvgOfMinValueInRowAndColumn);
        clock_gettime(CLOCK_REALTIME, &b);
    }
    else if(algorithm == MeasuerTSPAlgorithms(BnBMax)) {

        clock_gettime(CLOCK_REALTIME, &a);
        tsp.BnBDFS(&TSP::getMaxBound);
        clock_gettime(CLOCK_REALTIME, &b);
    }

    return _TimeDiff(a, b);
}



long Measure::_TimeDiff(timespec start, timespec end) {

    timespec temp;

    if ((end.tv_nsec - start.tv_nsec) < 0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}

    return temp.tv_sec * 1000000000 + temp.tv_nsec;
}
