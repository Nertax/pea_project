#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<algorithm>
#include<limits>
#include<queue>
#include<map>
#include<random>
#include<functional>

#ifndef Tsp_h
#define Tsp_h

struct TSPnode {

    uint8_t City;
    std::vector<uint8_t> CitiesLeft;
    int64_t Weight;
    int64_t Bound;
    std::vector<uint8_t> Path;

    bool operator<(const TSPnode &other) const {

        return Bound > other.Bound;
    }

    TSPnode(uint8_t city, std::vector<uint8_t> citiesLeft) : City(city), CitiesLeft(citiesLeft) {  ; }
    TSPnode(uint8_t city, std::vector<uint8_t> citiesLeft, std::vector<uint8_t> path) : City(city), CitiesLeft(citiesLeft), Path(path) {  ; }


};


struct CityWithLeftCities {

    uint8_t City;
    std::vector<uint8_t> CitiesLeft;

    bool operator<(const CityWithLeftCities &other) const { 
        
        if(City < other.City) return true;
        if(City > other.City) return false;

        if(CitiesLeft < other.CitiesLeft) return true;
        if(CitiesLeft > other.CitiesLeft) return false;

        return false;
    }

    CityWithLeftCities(uint8_t city, std::vector<uint8_t> citiesLeft) : City(city), CitiesLeft(citiesLeft) {  ; }

};

struct WeightAndPrevious {

    int64_t Weight;
    uint8_t PreviousCity; 

    WeightAndPrevious() = default;
    WeightAndPrevious(int64_t weight, uint8_t previousCity) : Weight(weight), PreviousCity(previousCity) { ; }

};

struct TSPresult {

    std::vector<uint8_t> shortestPath;
    int64_t pathWeight;

    std::string toString(uint8_t startCity);
};

class TSP {

    std::vector<std::vector<int>> citiesMatrix;
    uint8_t citiesAmount;
    uint8_t startCity {0};

    std::vector<uint8_t> getVectorWithoutElement(std::vector<uint8_t> vect, uint8_t value);
    bool ifVectorContain(std::vector<uint8_t> vect, uint8_t value);
    

public:
    void readMatrixFromFile(std::string fileName);
    void printCitiesMatrix();
    void setStartCity(uint8_t cityNumber);
    uint8_t getStartCity() { return startCity; }
    TSPresult bruteForce();
    TSPresult heldKarp();
    TSPresult BnBDFS(int64_t (TSP::*getBound)(TSPnode));
    void generateRandomCitiesMatrix(uint8_t matrixSize);
    int64_t getBoundByMinValueInEachRow(TSPnode node);
    int64_t getBoundByAvgOfMinValueInRowAndColumn(TSPnode node);
    int64_t getMaxBound(TSPnode node);


};

class Subset {

    static int Length;
    static std::vector<std::vector<uint8_t>> vectorOfSubsets;

public: 
    static void output(int string[], int position);
    static void generate(int string[], int position, int positions);
    static void setLength(int length);
    static std::vector<std::vector<uint8_t>> getVectorOfSubsets(int subsetSize);
    static void resetVectorOfSubsets() { vectorOfSubsets.clear(); }

};
#endif